class BookingsController < ApplicationController
  before_action :authenticate_user!
  before_action :check_time, :set_total
  before_action :set_hotel_category, only: [:new, :create, :available_rooms, :search_rooms]

  def show
    @bookings = Booking.find_by(id: params[:id] )
    @booking_details = @bookings.hotel_room.hotel_category
  end

  def new
  end

  def create
    @booking = current_user.bookings.new(booking_params)
    @condition = false
    hotel_room = HotelRoom.find_by(id: booking_params[:hotel_room_id])
    hotel_room.update(status: true)
    if @booking.save 
      BookingMailer.send_receipt(@booking, @booking.user.email).deliver_now
      @condition = true
    end
  end

  def edit
    @bookings = Booking.find_by(id: params[:id] )
    @booking_details = @bookings.hotel_room.hotel_category
  end

  def destroy
    @order = Booking.find(params[:id])
    @order.hotel_room.update(status: false)
    BookingMailer.send_destroy(@order, @order.user.email).deliver_now
    @order.destroy
    redirect_to bookings_path, notice: 'Your booking has been cancelled!'
  end

  def index
    @bookings = current_user.bookings.order(created_at: :desc ).page params[:page]
  end

  # Method for available rooms view
  def available_rooms
    @booking = current_user.bookings.new(booking_params)
    @hotel_rooms = @hotel_category.hotel_rooms.available_rooms(params[:booking][:check_in], params[:booking][:check_out])
  end

  # Method for search rooms view
  def search_rooms
  end

  def remove
    booking = Booking.find_by(id: params[:id])
    booking.destroy
    redirect_to bookings_path, notice: 'Your booking has been removed!'
  end

  private

  def set_hotel_category
    @hotel_category = HotelCategory.find(params[:hotel_category_id])
  end

  def booking_params
    params.require(:booking).permit(:check_in, :check_out, :hotel_room_id)
  end

  def set_total
    all_booking = Booking.all
    all_booking.each do |booking|
      booking_details = booking.hotel_room.hotel_category
      sub = booking_details.price
      stay_time = booking.booking_dates.size
      tax = (sub*stay_time*10/100)
      total = (sub*stay_time+tax)
      booking.update_attribute(:total_amount_cents, total)
    end
  end

  def check_time
    all_booking = Booking.all
    all_booking.each do |booking|
      time = booking.created_at.to_i + (2*60*60)
      now = Time.now.to_i
      if time - now < 0
        booking.hotel_room.update(status: false)
        booking.booking_dates.each do |booking_date|
          booking_date.destroy
        end
      end
    end
  end
end
