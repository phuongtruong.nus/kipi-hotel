class OrdersController < ApplicationController
	def express
		@order_details = Booking.find(params[:id])
		response = EXPRESS_GATEWAY.setup_purchase(@order_details.total_amount_cents,
			:ip                => request.remote_ip,
			:return_url        => new_booking_order_url(@order_details.id),
			:cancel_return_url => bookings_url,
			currency: "USD",
			allow_guest_checkout: true
		)

		redirect_to EXPRESS_GATEWAY.redirect_url_for(response.token)
	end

	def new
		@order = Order.new(:express_token => params[:token])
		@bookings = Booking.find( params[:booking_id] )
   		@booking_details = @bookings.hotel_room.hotel_category
	end

	def create
		booking = Booking.find(params[:booking_id])
	    @order = booking.build_order(order_params)
	    @order.ip_address = request.remote_ip
	    if @order.save
	      if @order.purchase
	      	BookingMailer.send_paid_details(booking,booking.user.email).deliver_now
	        render :action => "success"
	      else
	        render :action => "failure"
	      end
	    else
	      render :action => 'new'
	    end
	end

	private

	def order_params
		params.require(:order).permit(:express_token)
	end
end
