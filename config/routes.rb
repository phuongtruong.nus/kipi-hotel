Rails.application.routes.draw do  
  mount Ckeditor::Engine => '/ckeditor'
  devise_for :admins
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'
  # Added devise routes and overrided registration controller
  devise_for :users, :controllers => { registrations: 'registrations' }
  # hotel resources only with index route
  resources :hotel, only: :index do
    get :rooms, on: :collection
  end

  get '/remove/:id', to: 'bookings#remove', as: 'remove'
  # Listing our all the bookings of the user
  resources :bookings, only: [:index, :edit, :destroy, :show ]
  resources :services, only: :index
  get "dining", to: "hotel#foods", as: :dining  

  # Using resource hotel_categories for using the hotel_category_id for nested resources
  resources :hotel_categories, only: [] do
    # Using booking resources
    resources :bookings, only: [:new, :create] do 
      collection do
        get :search_rooms
        # route for checkig available_rooms
        get :available_rooms
      end
    end
  end
  resources :hotel_categories, only: [] do
    resources :hotel_rooms, only: [] do
      # Using booking resources
      resources :bookings, only: [:new, :create] do 
        collection do
          get :search_rooms
        end
      end
    end
  end

  # api routes
  namespace :api, defaults: {format: :json} do
    namespace :v1 do
      devise_for :users
      resources :hotel_rooms, only: :index
      resources :booking_informations, only: :index
    end
  end

  # root path 
  root "hotel#index"
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "/express_new_order/:id ", to: "orders#express", as: "express_new_order"
  resources :bookings, only: [:edit] do
    resources :orders
  end
end
