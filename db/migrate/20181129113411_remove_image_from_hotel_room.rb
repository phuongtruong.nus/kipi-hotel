class RemoveImageFromHotelRoom < ActiveRecord::Migration[5.0]
  def change
    remove_column :hotel_rooms, :image, :string
  end
end
