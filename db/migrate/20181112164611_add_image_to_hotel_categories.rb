class AddImageToHotelCategories < ActiveRecord::Migration[5.0]
  def change
    add_column :hotel_categories, :image, :string
  end
end
