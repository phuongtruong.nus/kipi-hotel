class AddStatusToHotelRoom < ActiveRecord::Migration[5.0]
  def change
  	add_column :hotel_rooms, :status, :boolean, default: false
  end
end
